// ESP32 Motor Controller for Encoder Calibration
/* This devices will act as a master for the SPI communication between the
encoder's esp32 and the TMC5160 motor controller. To communicate with
the TMC, the TMC5160.h library will be used. To communicate with the 
encoder, a basic custom communcation protocol is setup which is as follows:
    Convert int to binary:
    0 = Standby
    1 = Query
    100 = start calibration mode
    110 = step motor done
    102 = ready read encoder
    120 = read encoder done
    103 = finish calibration
    1000-2000 = send calibration info (Documentation found here: <insert link to documentation>)
        1000 = unused
        1001 - 1100 = nest max depth: 1-100, Default: 5, 1005
        1101 - 1200 = subdivisions: 1-100, Default: 3, 1103
        1201 - 1300 = first segments per loop: 1-100, Default: 6, 1206
        1301 - 1349 = prior lookup radius: 1-49, Default: 2, 1302
        1350 - 2000 = number of slits: 50-700, Default: 200, 1500

Using this protocol, the procedure to communicate the entire calibration
process is as follows:
    1. Master: ping slave (optional to check if slave exists)
        Slave: ack (If slave does not ack, then fail calibration (try again))
    2. Master: start calibration mode (change encoder to calibration mode)
        Slave: ack
    3. Master: send calibration info (optional parameters to be sent)
        Slave: ack
        * repeat as necessary *
    4. Master: step motor done (move motor to reset backlash and send done)
        Slave: read encoder done (reads the encoder values then send done)
        Master: ready motor (status update check on slave being done)
        Slave: ready read encoder (status update if not done)
    5. Master: step motor done (steps motor after reading "read encoder done" then send)
        Slave: read encoder done (reads the encoder values then send)
        Master: ready motor (status update check on slave being done)
        Slave: ready read encoder (status update if not done)
        * Repeat step 5 *
    6. Master: finish calibration (tells slave that calibration is wrapping up)
        Slave: ack (returns standby until slave is done, then ack)
        * Done *
 */

#include <Arduino.h>
#include <TMC5160.h>
#include <SPI.h>

// Encoder SPI pins
#define HSPI_MISO   12
#define HSPI_MOSI   13
#define HSPI_SCLK   14
#define HSPI_SS     33
// CS and enable pins for SPI communication with TMC
#define TMC_CS      5
#define TMC_EN      4

// SPI protocol
#define STANDBY     0
#define QUERY       1
#define START_CAL   100
#define DONE_STEP   110
#define READING_ENC 102
#define DONE_ENC    120
#define DONE_CAL    103

TMC5160_SPI motor = TMC5160_SPI(TMC_CS);

TMC5160_Reg::GCONF_Register gConf = { 0 };                // 0x00, Global configuration flags
TMC5160_Reg::RAMP_STAT_Register rampStatus = { 0 };       // 0x35, Ramp status and switch event status
TMC5160_Reg::IOIN_Register ioin = { 0 };                  // 0x04, Read input / write output pins
TMC5160_Reg::DRV_STATUS_Register drvStatus = { 0 };       // 0x6F, stallGuard2 value and driver error flags
TMC5160_Reg::SW_MODE_Register swMode = { 0 };             // 0x34, Switch mode configuration
TMC5160_Reg::IHOLD_IRUN_Register iHoldiRun= { 0 };        // 0x10, Driver current control
TMC5160_Reg::COOLCONF_Register coolConf= { 0 };           // 0x6D, coolStep smart current control register and stallGuard2 configuration

static const int SPI_CLOCK = 1000000; // 1 MHz
static const int SPI_DELAY = 1; // 1 millisecond
static const int SPI_TIMEOUT = 5 * 1000; // 5 seconds

static const float  MOTOR_GEAR_RATIO    = 50.8949725219; // the 51 to 1 gear ratio (triple planetary gearbox)
static const int    STEP_COUNT_PER_REV  = 200; // 200 full steps per 360 degrees
static const int    MICROSTEP_COUNT     = 256; // The microstepping setting
static const int    MICROSTEPS_PER_STEP = 1; // The number of microstepping per sample
static const float  OVERLAP_DEG         = 30; // How many degrees past 360 to collect
static const long   TOTAL_STEP_COUNT    = ceil(
    STEP_COUNT_PER_REV * MOTOR_GEAR_RATIO *
    MICROSTEP_COUNT / MICROSTEPS_PER_STEP *
    (360. + OVERLAP_DEG) / 360.
);

static bool MOTOR_TEST = false;
static unsigned long t_dirchange, t_echo;
static bool dir;
static int32_t target;

uint16_t EncoderCommand, EncoderRespond;
SPIClass *hspi = NULL;
SPISettings hspiSettings = SPISettings(SPI_CLOCK, MSBFIRST, SPI_MODE0);

// For motor testing purposes
uint32_t dir_change_time    = 10000;
uint32_t print_time         = 100;
float   test_target         = 5000;

void setup() {
    Serial.begin(250000);

    // Enable TMC
    pinMode(TMC_EN, OUTPUT);
    digitalWrite(TMC_EN, LOW);

    // This sets the motor & driver parameters /!\ run the configWizard for your driver and motor for fine tuning !
    TMC5160::PowerStageParameters powerStageParams; // defaults.
    TMC5160::MotorParameters motorParams;
    motorParams.globalScaler = 98; // Adapt to your driver and motor (check TMC5160 datasheet - "Selecting sense resistors")
    motorParams.irun = 31;
    motorParams.ihold = 16;

    hspi = new SPIClass(HSPI);

    hspi->begin(HSPI_SCLK, HSPI_MISO, HSPI_MOSI, HSPI_SS);
    SPI.begin();

    pinMode(HSPI_SS, OUTPUT);
    digitalWrite(HSPI_SS, HIGH);

    while (ioin.version != motor.IC_VERSION) {
        ioin.value = motor.readRegister(TMC5160_Reg::IO_INPUT_OUTPUT);

        if (ioin.value == 0 || ioin.value == 0xFFFFFFFF) {
            Serial.println("No TMC5160 found.");
            delay(2000);
        }
        else {
            Serial.println("Found a TMC device.");
            Serial.print("IC version: 0x");
            Serial.print(ioin.version, HEX);
            Serial.print(" (");
            if (ioin.version == motor.IC_VERSION) {
                Serial.println("TMC5160).");
            }
            else {
                Serial.println("unknown IC !)");
            }
        }
    }

    swMode.value = motor.readRegister(TMC5160_Reg::SW_MODE);
    swMode.en_softstop = 0;
    swMode.sg_stop = 1;
    swMode.pol_stop_r = 0;
    swMode.pol_stop_l = 0;
    swMode.stop_r_enable = 0;
    swMode.stop_l_enable = 0;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);

    coolConf.value = motor.readRegister(TMC5160_Reg::COOLCONF);
    coolConf.sfilt = 0;
    coolConf.sgt = 3; // less sensitive [-64..63] more sensitive
    motor.writeRegister(TMC5160_Reg::COOLCONF, coolConf.value);

    // ramp definition
    motor.begin(powerStageParams, motorParams, TMC5160::NORMAL_MOTOR_DIRECTION);

    motor.setRampMode(TMC5160::POSITIONING_MODE);
    motor.setCurrentPosition(0);
    motor.setMaxSpeed(400);
    motor.setRampSpeeds(0, 200, 200);
    motor.setAccelerations(500, 700, 1000, 1000);
    motor.setModeChangeSpeeds(20, 400, 150);

    Serial.println("starting up");
    print_help();
    // Standstill for automatic tuning
    delay(1000);
    // calibrate();
}

void print_help() {
    Serial.print("'t' to test motors");
    Serial.print("'r' reset stall");
    Serial.print("'c' to initiate calibration");
    Serial.print("'d' for delayed 30 seconds before calibrate");
    Serial.print("'h' display this help");
    Serial.println();
}

void loop() {
    if (MOTOR_TEST) {
        motorTest();
    }
    if (Serial.available() > 0) {
        String input = Serial.readStringUntil('\n');
        if (input == "") {
            input = "0";
        }
        if (input == "h") {
            print_help();
            return;
        }
        else if (input == "r") {
            swMode.sg_stop = 0;
            motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
            swMode.sg_stop = 1;
            motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
            return;
        }
        else if (input == "t") {
            MOTOR_TEST = !MOTOR_TEST;
            if (!MOTOR_TEST) {
                // Disable motors for now
                motor.setTargetPosition(motor.getCurrentPosition());
                dir = !dir;
                t_dirchange = 0;
                t_echo = 0;
            }
            return;
        }
        else if (input == "c") {
            calibrate();
            return;
        }
        else if (input == "d") {
            delay(30000);
            calibrate();
            return;
        }
        EncoderCommand = input.toInt();
        Serial.print("Command:");
        Serial.println(EncoderCommand);

        EncoderRespond = spiCommand(EncoderCommand);

        Serial.print("Response:");
        Serial.println(EncoderRespond);
    }
    // EncoderRespond = spiCommand(1);
    // Serial.print("Response:");
    // Serial.println(EncoderRespond);
    // delay(2000);
    // calibrate();
    delay(5);
}

void motorTest() {
    uint32_t now = millis();

    if (now - t_dirchange > dir_change_time || (now < dir_change_time && t_dirchange == 0)) {
        t_dirchange = now;

        // reverse direction
        dir = !dir;
        motor.setTargetPosition(dir ? test_target : 0);  // 1 full rotation = 200s/rev
    }
    // print out current position
    if (now - t_echo > print_time || (now < print_time && t_echo == 0)) {
        t_echo = now;

        // get the current target position
        float xactual = motor.getCurrentPosition();
        float vactual = motor.getCurrentSpeed();
        Serial.print("X:"); Serial.print(xactual/200*36, DEC); Serial.print(", ");
        Serial.print("V_rpm:"); Serial.print(vactual/200*60, DEC); Serial.print(", ");
        Serial.print("V_sps:"); Serial.print(vactual, DEC); Serial.print(", ");

        drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
        Serial.print("StallGuardValue:"); Serial.print(drvStatus.sg_result, DEC); Serial.print(", ");
        Serial.print("StallGuardStatus:"); Serial.print(drvStatus.stallguard ? true:false); Serial.print(", ");
        Serial.print("Actual_motor_current:"); Serial.print(drvStatus.cs_actual, DEC); Serial.print(", ");
        // uint32_t tstep = motor.readRegister(TMC5160_Reg::TSTEP);
        // float tstep_speed = 12000000.0/(256.0*tstep);
        // Serial.print("tstep_speed:"); Serial.print(tstep_speed, DEC); Serial.print(", ");
        // Serial.print("TSTEP:"); Serial.print(motor.readRegister(TMC5160_Reg::TSTEP), DEC); Serial.print(", ");
        // Serial.print("TPWMTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TPWMTHRS), DEC); Serial.print(", ");
        // Serial.print("TCOOLTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TCOOLTHRS), DEC); Serial.print(", ");
        // Serial.print("THIGH:"); Serial.print(motor.readRegister(TMC5160_Reg::THIGH), DEC); Serial.print(", ");

        rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
        Serial.print("position_reached:"); Serial.print(rampStatus.position_reached ? true : false); Serial.print(", ");
        Serial.println();
        // Serial.print("Status Stop L : ");
        // Serial.println(rampStatus.status_stop_l ? true : false);
        // Serial.print("Status Stop R : ");
        // Serial.println(rampStatus.status_stop_r ? true : false);

        // Serial.print("Status Velocity Reaced : ");
        // Serial.println(rampStatus.velocity_reached ? true : false);
        // Serial.print("Status SG : ");
        // Serial.println(rampStatus.status_sg ? true : false);
    }
}

int calibrate() {
    Serial.println("*** Beginning Calibration ***");
    // send start calibration mode
    EncoderRespond = spiCommand(START_CAL);
    if (EncoderRespond != START_CAL) {
        Serial.print("Encoder not responding: ");
        Serial.print(EncoderRespond);
        Serial.println(" Exiting calibration, try again.");
        return 1;
    }
    // send calibration info 

    // remove backlash
    Serial.println("Resetting motor position to remove backlash.");
    motor.setCurrentPosition(0);
    motor.setTargetPosition(1000);
    delay(10);
    rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
    while (rampStatus.position_reached ? false : true) {
        delay(1);
        rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
    }
    motor.setCurrentPosition(0);
    // step 4 step motor done
    // Serial.println(EncoderRespond);
    EncoderRespond = spiCommand(DONE_STEP, DONE_ENC);
    Serial.println(EncoderRespond);
    Serial.print("TOTAL_STEP_COUNT: ");
    Serial.println(TOTAL_STEP_COUNT);

    long i = 0;
    while(i < TOTAL_STEP_COUNT) {
        if (EncoderRespond == DONE_ENC) {
            // Step motor
            target += MICROSTEPS_PER_STEP;
            motor.writeRegister(TMC5160_Reg::XTARGET, target);
            // Wait till step done
            rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
            while (rampStatus.position_reached ? false : true) {
                delay(5);
                // Serial.print(".");
                rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
            }
            // Send done step
            EncoderRespond = spiCommand(DONE_STEP, DONE_ENC);
            // Serial.print("DONE_STEP ");
            // Serial.println(EncoderRespond);
            // only increment when the motor has been stepped
            i++;
            uint32_t t_now = millis();
            static unsigned long t_echo;
            if (t_now - t_echo > 1000) {
                t_echo = t_now;
                Serial.print("Progress: (");
                Serial.print(uint64_t(100000.0*i/TOTAL_STEP_COUNT)/1000.0);
                Serial.print("%) ");
                Serial.print(i);
                Serial.print(" / ");
                Serial.println(TOTAL_STEP_COUNT);
            }
        } // EncoderRespond == DONE_ENC
        else {
            //  Just print out status messages
            //  Serial.print("Encoder Status: ");
            //  Serial.println(EncoderRespond);
            //  ping status
            EncoderRespond = spiCommand(QUERY, DONE_ENC);
            // Serial.print("READY_STEP ");
            // Serial.println(EncoderRespond);
            delay(SPI_DELAY);
        }
    }
    // Finished calibration
    unsigned long start_time = millis();
    EncoderRespond = spiCommand(DONE_CAL);
    unsigned long timer = millis() - start_time;
    while (EncoderRespond != DONE_CAL && timer < SPI_TIMEOUT) {
        delay(SPI_DELAY);
        EncoderRespond = spiCommand(DONE_CAL);
        timer = millis() - start_time;
    }
    if (timer >= SPI_TIMEOUT) {
        Serial.println("Calibration failed to finish! Check cal files for errors");
        return 1;
    }
    Serial.println("*** End Calibration ***");
    return 0;
}

// bool ping() {
//     unsigned long start_time = millis();
//     uint16_t respond = spiCommand(1);
//     unsigned long timer = millis() - start_time;

//     Serial.print("ping: ");
//     if (timer >= SPI_TIMEOUT) {
//         Serial.print("Timeout.");
//     }
//     else {
//         Serial.print(timer);
//     }
//     Serial.println();
//     if (respond != STANDBY) {
//         return true;
//     }
//     return false;
// }

// Sends command to encoder and pings every SPI_DELAY milliseconds for a response
// Continue to ping until response is not 0 (Standby) for up to SPI_TIMEOUT seconds
uint16_t spiCommand(uint16_t command) {
    uint16_t respond = STANDBY;
    unsigned long start_time = millis();
    unsigned long timer = millis() - start_time;

    hspi->beginTransaction(hspiSettings);
    digitalWrite(HSPI_SS, LOW);
    uint16_t old_respond = hspi->transfer16(command);
    digitalWrite(HSPI_SS, HIGH);
    hspi->endTransaction();
    delay(SPI_DELAY);
    // Serial.print("master SPI message ");
    // Serial.print(command);
    // Serial.print(" ");
    // Serial.print(respond);
    // Serial.println();
    while(timer < SPI_TIMEOUT) {
        hspi->beginTransaction(hspiSettings);
        digitalWrite(HSPI_SS, LOW);
        respond = hspi->transfer16(QUERY);
        digitalWrite(HSPI_SS, HIGH);
        hspi->endTransaction();

        // Serial.print("master SPI message ");
        // Serial.print(QUERY);
        // Serial.print(" ");
        // Serial.print(respond);
        // Serial.println();
        if (respond != old_respond) {
            break;
        }
        delay(SPI_DELAY);
        timer = millis() - start_time;
    }
    if (timer >= SPI_TIMEOUT) {
        Serial.print("Timeout. Response was 0 every query for command: ");
        Serial.println(command);
    }
    return respond;
}

uint16_t spiCommand(uint16_t command, uint16_t desired_response) {
    uint16_t respond = STANDBY;
    unsigned long start_time = millis();
    unsigned long timer = millis() - start_time;

    hspi->beginTransaction(hspiSettings);
    digitalWrite(HSPI_SS, LOW);
    respond = hspi->transfer16(command);
    digitalWrite(HSPI_SS, HIGH);
    hspi->endTransaction();
    delay(SPI_DELAY);
    // Serial.print("master SPI message ");
    // Serial.print(command);
    // Serial.print(" ");
    // Serial.print(respond);
    // Serial.println();
    while(timer < SPI_TIMEOUT) {
        hspi->beginTransaction(hspiSettings);
        digitalWrite(HSPI_SS, LOW);
        respond = hspi->transfer16(QUERY);
        digitalWrite(HSPI_SS, HIGH);
        hspi->endTransaction();

        // Serial.print("master SPI message ");
        // Serial.print(QUERY);
        // Serial.print(" ");
        // Serial.print(respond);
        // Serial.println();
        if (respond == desired_response) {
            break;
        }
        delay(SPI_DELAY);
        timer = millis() - start_time;
    }
    if (timer >= SPI_TIMEOUT) {
        Serial.print("Timeout. Response was 0 every query for command: ");
        Serial.println(command);
    }
    return respond;
}
